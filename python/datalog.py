"""
DataLogger wraps existing Logger objects to allow additional keywords. LogRecords
store a Message object containing the log message and any optional keywords.
Message objects format themselves when cast to string to allow formatting to be
delayed until required.

When the first DataLogger logs, it will initialise the MasterOutput assigned to
the class. MasterOutput controls how the expected log output is constructed using
it's assigned PathManager. PathManager formats the log output paths and can be
used to search for paths.

Each log output line is, by default, a json string. Payload objects are used to
serialise and deserialise the strings, and provide methods for accessing log
attributes and formatting the logs. Utility methods allow iterating over the
payloads in a file, and filtering the payloads.
"""
import datetime
import errno
import getpass
import glob
import json
import logging
import logging.handlers
import os
import sys
import tempfile
import threading
import traceback


PREFIX = "user."
DEFAULT_FORMAT_FIELDS = ("levelname", "created", "name", "msg")
DEFAULT_COLOURS = {
    0: "\033[37m",  # Light Gray
    20: "\033[39m",  # White
    30: "\033[93m",  # Light Yellow
    40: "\033[91m",  # Light Red
    50: "\033[31m",  # Red
}
COLOUR_TERMINATE = "\033[0m"


# ============================================================================ #
#  Output
# ============================================================================ #


class DataLogError(Exception):
    """ Errors raised by datalog """


def get_launch_script():
    """
    Returns:
        str: Name of the script used to start the process
    """
    path = sys.argv[0]
    package, _ = os.path.splitext(os.path.basename(path))
    return package


class LogPathManager(object):
    # Values that are constant to the current session. Exposed via class methods
    # so that sub-classes may implement custom functionality, including any
    # processing logic (that would not be possible with class attributes)
    DATE_FMT = "%Y-%m-%d"
    _CONST_DATE = datetime.datetime.now().strftime(DATE_FMT)
    _CONST_PACKAGE = get_launch_script()
    _CONST_PATTERN = "{root}/{date}/{user}/{package}/{process}.{thread}.log"
    _CONST_PID = os.getpid()
    _CONST_ROOT = tempfile.gettempdir()
    _CONST_USER = getpass.getuser()

    # ======================================================================== #
    #  Constants
    # ======================================================================== #

    @classmethod
    def date(cls):
        return cls._CONST_DATE

    @classmethod
    def package(cls):
        return cls._CONST_PACKAGE

    @classmethod
    def pattern(cls):
        return cls._CONST_PATTERN

    @classmethod
    def pid(cls):
        return cls._CONST_PID

    @classmethod
    def root(cls):
        return cls._CONST_ROOT

    @classmethod
    def thread(cls):
        return threading.get_ident()

    @classmethod
    def user(cls):
        return cls._CONST_USER

    # ======================================================================== #
    #  Utility
    # ======================================================================== #

    @classmethod
    def format(
        cls, date=None, package=None, pid=None, root=None, thread=None, user=None
    ):
        return os.path.abspath(
            cls.pattern().format(
                date=date or cls.date(),
                package=package or cls.package(),
                process=pid or cls.pid(),
                root=root or cls.root(),
                thread=thread or cls.thread(),
                user=user or cls.user(),
            )
        )

    @classmethod
    def ls(cls, date=None, package=None, pid=None, root=None, thread=None, user=None):
        wildcard = "*"
        fs_pattern = cls.format(
            date=date or wildcard,
            package=package or wildcard,
            pid=pid or wildcard,
            root=root,  # Root cannot be wildcarded
            thread=thread or wildcard,
            user=user or wildcard,
        )
        return glob.iglob(fs_pattern)

    @classmethod
    def latest(cls, days=7, package=None, pid=None, root=None, thread=None, user=None):
        now = datetime.datetime.now()
        for i in range(days):
            date = (now - datetime.timedelta(days=i)).strftime(cls.DATE_FMT)
            paths = list(
                cls.ls(
                    date=date,
                    package=package,
                    pid=pid,
                    root=root,
                    thread=thread,
                    user=user,
                )
            )
            if paths:
                mapped = {os.path.getmtime(path): path for path in paths}
                return mapped[max(mapped)]
        return ""


class MasterOutput(object):
    _MASTER_HANDLER = None
    _MASTER_LOG_PATH = None

    PathManager = LogPathManager

    @classmethod
    def handler(cls):
        """
        Warnings:
            Initialises the output if not already initialised

        Returns:
            logging.Handler: Handler used by the output.
        """
        if cls._MASTER_HANDLER is None:
            cls.initialise()
        return cls._MASTER_HANDLER

    @classmethod
    def path(cls):
        """
        Warnings:
            Initialises the output if not already initialised

        Returns:
            str: Path the output is written to
        """
        if cls._MASTER_LOG_PATH is None:
            cls.initialise()
        return cls._MASTER_LOG_PATH

    @classmethod
    def is_initialised(cls):
        """
        Returns:
            bool: Whether or not the output has been initialised
        """
        return cls._MASTER_HANDLER is not None or cls._MASTER_LOG_PATH is not None

    @classmethod
    def initialise(cls, path=None):
        """
        Initialises a default handler and path for logging output

        Raises:
            DataLogError: If output is already initialised

        Keyword Args:
            path (str): Where to log the output to. If not given, the default
                PathManager format is used
        """
        if cls.is_initialised():
            raise DataLogError("Logging already initialised")

        if path is None:
            path = cls.PathManager.format()
            directory = os.path.dirname(path)
            try:
                os.makedirs(directory)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise

        root_logger = getLogger()
        cls._MASTER_LOG_PATH = path
        cls._MASTER_HANDLER = logging.handlers.TimedRotatingFileHandler(
            path, when="midnight", backupCount=9, utc=True
        )
        root_logger.log_to_file(path, handler=cls._MASTER_HANDLER)
        root_logger.debug("Logging initialised")

    @classmethod
    def redirect(cls, path):
        """
        Raises:
            DataLogError: If output is not yet initialised

        Args:
            path (str): Where to move the logging output to.
        """
        if cls._MASTER_LOG_PATH is None:
            raise DataLogError("Logging has not been initialised, cannot move")

        previous_path = cls._MASTER_LOG_PATH

        root_logger = getLogger()
        root_logger.info("Moving logging to {path}", path=path)
        for handler in root_logger.handlers:
            if handler == cls._MASTER_HANDLER:
                root_logger.removeHandler(cls._MASTER_HANDLER)
                break

        cls._MASTER_LOG_PATH = path
        cls._MASTER_HANDLER = logging.handlers.TimedRotatingFileHandler(
            path, when="midnight", backupCount=9, utc=True
        )
        root_logger.log_to_file(path, handler=cls._MASTER_HANDLER)
        root_logger.info("Logging moved from {path}", path=previous_path)


# ======================================================================== #
#  Logger
# ======================================================================== #


def format_exc_info(exc_info):
    """
    Args:
        exc_info (tuple): Exception info as returned by `sys.exc_info()`

    Returns:
        list[str]: Exception information broken down into a json format-able list
    """
    exc_type, exc_value, exc_tb = exc_info
    return [
        exc_type.__name__,  # Exception class name
        str(exc_value),  # Exception message
        [list(i) for i in traceback.extract_tb(exc_tb)],  # Python 2/3 compatible
    ]


# TODO: Should be sub-classable in some way
def get_additional_log_fields():
    """
    Returns:
        dict: Dictionary of additional fields to merge into the log payload
    """
    return {"username": getpass.getuser()}


class Payload(object):
    @classmethod
    def deserialise(cls, string):
        """
        Args:
            string (str): Json string

        Returns:
            Payload:
        """
        data = json.loads(string)
        return cls(data)

    @classmethod
    def from_logrecord(cls, record):
        """
        Args:
            record (logging.LogRecord): LogRecord constructed by a Logger

        Returns:
            Payload:
        """
        # Take a copy of the record so we can modify the exc_info/Message
        data = record.__dict__.copy()
        if isinstance(record.msg, Message):
            data["msg"] = record.msg.as_dict()
        if record.exc_info:
            data["exc_info"] = format_exc_info(record.exc_info)
        return cls(data)

    def __init__(self, data):
        """
        Args:
            data (dict):
        """
        self.data = data

    def colour(self):
        """
        Returns:
            str: Terminal colour string to use on the line
        """
        level = self.data.get("levelno", 0)
        match = ""
        for value, colour in sorted(DEFAULT_COLOURS.items()):
            if level < value:
                return match
            match = colour
        return match

    def format_field(self, field_name):
        """
        Args:
            field_name (str): Name of a key from the LogRecord. User keys are
                prefixed.

        Returns:
            str: Formatted version of the field or an empty string
        """
        if field_name in ("msg", "message"):
            msg = self.data.get("msg")
            value = (
                Message(msg["message"], msg["kwargs"]) if isinstance(msg, dict) else msg
            )
        elif field_name.startswith(PREFIX):
            key = field_name[len(PREFIX) :]
            user_data = self.user_data()
            if key not in user_data:
                return ""
            value = user_data.get(key)
        else:
            if field_name not in self.data:
                return ""
            value = self.data.get(field_name)

        if field_name == "created":
            return datetime.datetime.utcfromtimestamp(value).strftime(
                "%d-%m-%y %H:%M:%S"
            )
        else:
            return str(value)

    def format(self, fields=None, colours=False, tracebacks=False):
        """
        Keyword Args:
            fields (list[str]): List fo fields to format into the string. User
                fields are prefixed.
            colours (bool): Whether or not to include terminal colours
            tracebacks (bool): Whether or not to include formatted tracebacks

        Returns:
            str: Payload formatted as a string
        """
        string = " ".join(
            (self.format_field(name) for name in fields or DEFAULT_FORMAT_FIELDS)
        )
        if tracebacks:
            tb = self.traceback()
            if tb:
                string = "\n".join((tb, string))
        # Only add colours to valid strings
        if colours and string:
            string = "{}{}{}".format(self.colour(), string, COLOUR_TERMINATE)
        return string

    def has_key(self, key):
        """
        Args:
            key (str): Field name in the payload. User keys are prefixed.

        Returns:
            bool: Whether or not the key exists in the Payload
        """
        if key.startswith(PREFIX):
            return key[len(PREFIX) :] in self.user_data()
        else:
            return key in self.data

    def serialise(self):
        """
        Returns:
            str: Data dumped as a json string
        """
        return json.dumps(self.data, default=repr)

    def traceback(self):
        """
        Returns:
            str: Any exception information in the Payload formatted as a string
        """
        exc_info = self.data.get("exc_info")
        return "".join(traceback.format_list(exc_info[2])) if exc_info else ""

    def user_data(self):
        """
        Returns:
            dict: Dictionary of user supplied fields
        """
        msg = self.data["msg"]
        return msg.get("kwargs", {}) if isinstance(msg, dict) else {}


class Message(object):
    """ Log message that accepts keyword arguments """

    def __init__(self, message, kwargs):
        """
        Args:
            message (str):
            kwargs (dict):
        """
        self.message = message
        self.kwargs = kwargs

    def as_dict(self):
        """
        Returns:
            dict:
        """
        return {"message": self.message, "kwargs": self.kwargs}

    def __str__(self):
        return self.message.format(**self.kwargs) if self.kwargs else self.message


class DataLogger(logging.Logger):
    """
    Wraps a normal logger by cloning it's current state.
    Overloads the internal logging mechanism to allow keywords for new style
    formatting and adds convenience methods.
    """

    Output = MasterOutput

    def __init__(self, base_logger):
        """
        Args:
            base_logger (logging.Logger):
        """
        super(DataLogger, self).__init__(base_logger.name, level=base_logger.level)
        # Clone the attributes of the target logger to preserve the logger hierarchy
        self.__dict__ = base_logger.__dict__.copy()

    def _log(self, level, msg, args, exc_info=None, extra=None, **kwargs):
        if self.Output is not None and not self.Output.is_initialised():
            self.Output.initialise()

        msg = Message(msg, kwargs)
        # Include additional log information
        extra = extra or {}
        extra.update(get_additional_log_fields())
        return super(DataLogger, self)._log(
            level, msg, args, exc_info=exc_info, extra=extra
        )

    def log_to_file(
        self, filepath, mode="a", encoding=None, delay=0, level=None, handler=None
    ):
        """ Calls datalog.log_to_file for this logger """
        return log_to_file(
            self,
            filepath,
            mode=mode,
            encoding=encoding,
            delay=delay,
            level=level,
            handler=handler,
        )

    def log_to_stream(self, fmt=None, level=None, stream=None, handler=None):
        """ Calls datalog.log_to_stream for this logger """
        return log_to_stream(self, fmt=fmt, level=level, stream=stream, handler=handler)


class DataFormatter(logging.Formatter):
    """
    Writes the Log Record as a json string. This is compatible with the
    standard library Logger as well as DataLogger.
    """

    def format(self, record):
        """
        Args:
            record (logging.LogRecord):

        Returns:
            str: Record dumped as a serialised string
        """
        payload = Payload.from_logrecord(record)
        return payload.serialise()


def getLogger(name=None):
    """
    Gets the Logger as a DataLogger - level is always set to DEBUG to ensure
    all logs are propagated.

    Keyword Args:
        name (str): Name of the logger to retrieve, defaults to root logger

    Returns:
        DataLogger: Existing logging.Logger wrapped in a DataLogger
    """
    logger = logging.getLogger(name)
    data_logger = DataLogger(logger)
    data_logger.setLevel(logging.DEBUG)
    data_logger.propagate = True
    return data_logger


def log_to_file(
    logger, filepath, mode="a", encoding=None, delay=0, level=None, handler=None
):
    handler = handler or logging.FileHandler(
        filepath, mode=mode, encoding=encoding, delay=delay
    )
    handler.setFormatter(DataFormatter())
    if level is not None:
        handler.setLevel(level)
    logger.addHandler(handler)
    return handler


def log_to_stream(logger, fmt=None, level=None, stream=None, handler=None):
    handler = handler or logging.StreamHandler(stream=stream)
    if fmt is not None:
        formatter = logging.Formatter(fmt)
        handler.setFormatter(formatter)
    if level is not None:
        handler.setLevel(level)
    logger.addHandler(handler)
    return handler


# ============================================================================ #
#  Reader
# ============================================================================ #


def iter_log_file(filepath, ignore_errors=False, payload_cls=Payload):
    """
    Args:
        filepath (str): Filepath to read the logs from

    Keyword Args:
        ignore_errors (bool): Whether or not to ignore invalid lines
        payload_cls (Type[Payload]): Class to wrap the log lines in.

    Returns:
        Iterable[Payload]: Generator of payloads
    """
    with open(filepath, "r") as f:
        for line in f:
            try:
                yield payload_cls.deserialise(line)
            except Exception:
                if not ignore_errors:
                    raise


def filter_payloads(payloads, filter_fields=None, min_level=None, max_level=None):
    """
    Args:
        payloads (Iterable[Payload]): Any iterable of payload objects

    Keyword Args:
        filter_fields (list[str]): List of field names the Payload must have.
            User keys are prefixed.
        min_level (int): Minimum log level to be yielded
        max_level (int): Maximum log level to be yielded

    Returns:
        Iterable[Payload]: Iterable of filtered payloads
    """
    min_level = min_level or min(DEFAULT_COLOURS)
    max_level = max_level or max(DEFAULT_COLOURS)
    for payload in payloads:
        if min_level <= payload.data.get("levelno") <= max_level and (
            not filter_fields or any(payload.has_key(key) for key in filter_fields)
        ):
            yield payload
